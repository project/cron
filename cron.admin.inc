<?php

/**
 * @file
 * Cron module admin functions.
 */

/**
 * Settings form.
 */
function cron_settings_admin() {
  $period = drupal_map_assoc(array(0, 60, 180, 300, 600, 900, 1800, 2700, 3600, 10800, 21600, 32400, 43200, 86400), 'format_interval');
  $period[0] = '<'. t('disabled') .'>';
  $form['cron_disabled_threshold'] = array(
    '#type' => 'select',
    '#title' => t('Show error when cron has not run for'),
    '#default_value' => variable_get('cron_disabled_threshold', 2700),
    '#options' => $period,
  );

  return system_settings_form($form);
}
